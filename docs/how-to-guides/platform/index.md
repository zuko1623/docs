---
title: Database Lab Platform
sidebar_label: Overview
slug: /how-to-guides/platform
---

## Guides
- [Start using Database Lab Platform](/docs/how-to-guides/platform/start-using-platform)
- [Create and use Database Lab Platform tokens](/docs/how-to-guides/platform/tokens)
- [Database Lab Platform onboarding checklist](/docs/how-to-guides/platform/onboarding)
