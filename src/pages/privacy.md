---
title: Privacy Policy
---

Terms of Service: https://postgres.ai/tos/

# Privacy policy

*Last Updated: May 27, 2021*

This Privacy Policy (this “Policy”) is adopted by Nombox LLC d.b.a. Postgres.ai located at 1148 Carlos Privada, Mountain View, CA 94040, USA ("Postgres.ai" or "we").

Postgres.ai has the utmost respect for your privacy, and thus, we want to give you the best possible experience while you are using our website. Hence, please take your time to review this Policy. Your continued use of our website and our services will consitute your representation that you have read, understood and agreed to this Policy.

Postgres.ai will not disclose or sell your personal information, without your consent, to third parties who are not affiliated with Postgres.ai except as otherwise provided below in this Policy.

We believe that you have the right to know what kind and how information may be collected by this website. Moreover, you ought to know how this information may be used, disclosed, and the amount of protection we instill upon your privacy.

This Policy applies to our customers and users who have visited our website and inquired about or bought our services, as well as those who have visited and used the Postgres.ai website (https://postgres.ai/) without purchasing our services.

## INFORMATION WE COLLECT
- **Your personal details** – name, address, phone number, email address when you submit web forms on our website, including opportunities to sign up for and agree to receive email communications from us. We also may ask you to submit such personal information if you choose to use interactive features of our website, including participation in surveys, contests, promotions, sweepstakes, requesting customer support, registration for attendance at an event sponsored by Postgres.ai or otherwise communicating with us.
- **Log Files** – certain information including but is not limited to Internet Protocol (IP) addresses, system configuration information, URLs of referring pages, and locale and language preferences when you visit and interact with most websites and services delivered via the Internet, when you visit our website and interactive areas offered by it.
- **Cookies and Other Tracking Technologies** – we may use cookies to provide you with a user-friendly interface, features and general access to our website. You can control how our website use cookies by configuring your browser's privacy settings (please refer to your browser's help function to learn more about cookie controls). Note that if you disable cookies entirely, our website may not function properly. Third parties may use the data collected by the cookies. The purpose of the use of cookies on our website is site improvement, as well as the analysis of our customer-provider relationships and marketing intentions. For more information about the cookies Postgres.ai uses, please see our Cookie Policy.
- **Interactive Areas** – publicly accessible blogs, chats, community forums, comments sections, discussion forums, or other interactive features (“Interactive Areas”). If you choose to participate in any of these Interactive Areas, please be aware that that any information that you post in an Interactive Area might be read, collected, and used by others who access it. If you wish to remove your personal information from any of our Interactive Areas, please contact us at: privacy@postgres.ai.

## HOW WE USE INFORMATION WE COLLECT
Postgres.ai only processes personal information in a way that is compatible with and relevant for the purpose for which it was collected or authorized. As a general matter, for all categories of data we collect, we may use the information we collect to:

- provide, operate, maintain, improve, and promote our website and services;
- enable you to access and use our website and services;
- process and complete transactions, and send you related information, including purchase confirmations and invoices;
- send transactional messages, including responses to your comments, questions, and requests; provide customer service and support; and send you technical notices, updates, security alerts, and support and administrative messages;
- send commercial communications, in accordance with your communication preferences, such as providing you with information about products and services, features, surveys, newsletters, offers, promotions, contests, and events about us and our partners; and send other news or information about us and our partners.
- monitor and analyze trends, usage, and activities in connection with our website and Services and for marketing or advertising purposes;
- comply with legal obligations as well as to investigate and prevent fraudulent transactions, unauthorized access to the Services, and other illegal activities;
- personalize our website and Services, including by providing features or content that match your interests and preferences; and
- process for other purposes for which we obtain your consent.

## INFORMATION SECURITY AND ACCURACY
We intend to protect your personal information and to maintain its accuracy. Postgres.ai implements reasonable physical, administrative and technical safeguards to help us protect your personal information from unauthorized access, use and disclosure. For example, we encrypt passwords when stored and encrypt all and personal information when we transmit such information over the Internet. We also require that our suppliers protect information from unauthorized access, use and disclosure.

## INFORMATION SHARING
We work with other companies who help us run our business (“Service Providers”). Service Providers provide services to help us deliver customer support, process credit card payments, manage and contact our existing Customers as well as sales leads, provide marketing support, and otherwise operate and improve our Services. These Service Providers may only process personal information pursuant to our instructions and in compliance both with this Privacy Policy and other applicable confidentiality and security measures and regulations.

Specifically, we do not permit our Service Providers to use any personal information we share with them for their own marketing purposes or for any other purpose other than in connection with the services they provide to us.

You can obtain a list of our Service Providers on request at privacy@postgres.ai.

In addition to sharing with Service Providers as described above, we may also share your information with others in the following circumstances:

- With our partners who may help us distribute the Services to Customers
- In the event of a merger, sale, change in control, or reorganization of all or a part of our business
- When we are required to disclose personal information to respond to subpoenas, court orders, or legal process, or to establish or exercise our legal rights or defend against legal claims
- Where we have a good-faith belief sharing is necessary to investigate, prevent, or take action regarding illegal activities, suspected fraud, situations involving potential threats to the physical safety of any person, violations of our Service Agreement, or as otherwise required to comply with our legal obligations; or
- In other situations where you provide your consent from time to time.

We do not sell, rent, or share personal information with third parties for their direct marketing purposes.

## PERSONAL DATA OF CHILDREN
Postgres.ai’s services are not designed for users who are under the age of 16. Hence, we will never knowingly collect, use, or process personal data provided by someone who is under the age of 16.

## CHANGES TO THIS PRIVACY POLICY
We recognize that transparency is an ongoing responsibility so we will keep this Policy under regular review.

This Policy was last updated on May 26, 2021.

## DATA CONTROLLER AND CONTACT INFORMATION
If you have any questions about this Policy or how and why we process personal data, please contact us at:

```
NOMBOX LLC d.b.a. Postgres.ai
1148 Carlos Privada, Mountain View, CA 94040, USA
Email: privacy@postgres.ai
```

## INDIVIDUALS’ RIGHTS AND HOW TO EXERCISE THEM

Individuals have certain rights over their personal data and controllers are responsible for fulfilling these rights.

Individuals’ rights may include the right of access to personal data, to rectification of personal data, to erasure of personal data / right to be forgotten, to restrict processing of personal data, to object to processing of personal data, to data portability, the right to withdraw consent at any time (where processing is based on consent) and the right to lodge a complaint with a supervisory authority.

Please see further information about these rights, when they are available and how to exercise them below.

### Your right of access to personal data
You have the right to obtain confirmation as to whether we process personal data about you, receive a copy of your personal data held by us as a controller and obtain certain other information about how and why we process your personal data (similar to the information provided in this privacy statement). This right may be exercised by emailing us at privacy@postgres.ai. We aim to respond to any requests for information promptly, and in any event within the legally required time limits.

### Your right to rectification / amendment of personal data
You have the right to request for your personal data to be amended or rectified where it is inaccurate (for example, if you change your name or address) and to have incomplete personal data completed.

To update personal data submitted to us, you may email us at privacy@postgres.ai or, where appropriate, contact us via the relevant website registration page or directly amend the personal details held on relevant websites or applications with which you registered.

When practically possible, once we are informed that any personal data processed by us is no longer accurate, we will make updates as appropriate based on your updated information.

### Your right to erasure / right to be forgotten
You have the right to obtain deletion of your personal data in the following cases:

- the personal data are no longer necessary in relation to the purposes for which they were collected and processed;
- our legal grounds for processing is consent, you withdraw consent and we have no other lawful basis for the processing;
- our legal grounds for processing is that the processing is necessary for legitimate interests pursued by us or a third party, you object to our processing and we do not have overriding legitimate grounds;
- you object to our processing for direct marketing purposes;
- your personal data have been unlawfully processed; or
- your personal data must be erased to comply with a legal obligation to which we are subject.

To request deletion of your personal data, please email us at privacy@postgres.ai

### Your right to restrict processing

You have the right to restrict our processing of your personal data in the following cases:

- for a period enabling us to verify the accuracy of your personal data where you have contested the accuracy of the personal data;
- your personal data have been unlawfully processed and you request restriction of processing instead of deletion;
- your personal data are no longer necessary in relation to the purposes for which they were collected and processed but the personal data are required by you to establish, exercise or defend legal claims; or
- for a period enabling us to verify whether the legitimate grounds relied on by us override your interests where you have objected to processing based on it being necessary for the pursuit of a legitimate interest identified by us.

To restrict our processing of your personal data, please email us at privacy@postgres.ai

### Your right to object to processing
You have the right to object to our processing of your personal data in the following cases:

- our legal grounds for processing is that the processing is necessary for a legitimate interest pursued by us or a third party; or
- our processing is for direct marketing purposes.

To object to our processing of your personal data, please email us at privacy@postgres.ai

### Your right to data portability

You have a right to receive your personal data provided by you to us and have the right to send the data to another organization (or ask us to do so if technically feasible) where our lawful basis for processing the personal data is consent or necessity for the performance of our contract with you and the processing is carried out by automated means.

To exercise your right to data portability, please email us at privacy@postgres.ai

### Your right to withdraw consent

Where we process personal data based on consent, individuals have a right to withdraw consent at any time. We do not generally process personal data based on consent (as we can usually rely on another legal basis). Where we rely on your consent for our processing of your personal data, to withdraw your consent please email us at privacy@postgres.ai or, to stop receiving an email from a Postgres.ai marketing list.

You may manage your receipt of commercial communications by clicking on the “unsubscribe” link located on the bottom of such emails, through your account settings if you have a Postgres.ai account, or you may send a request to unsubscribe@postgres.ai

### Complaints

We hope that you won’t ever need to, but if you do want to complain about our use of personal data, please send an email with the details of your complaint to privacy@postgres.ai. We will look into and respond to any complaints we receive.

You also have the right to lodge a complaint with the supervisory authority in your country of residence, place of work or the country in which an alleged infringement of data protection law has occurred within the EU.

---

***If you have any questions or comments about this Privacy Policy, please contact us at privacy@postgres.ai.***
